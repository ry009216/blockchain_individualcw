﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Threading;
using System.Timers;
//using System.Diagnostics;

namespace BlockchainAssignment
{
    class Block
    {
        //all the properties of a block
        DateTime creationT;
        String currentHash, previousHash, minerAddress, merkleRoot;
        List<transcations> transactionList = new List<transcations>();
        int nonce, difficulty,hex, positionInx;
        double rewards, fees;
        TimeSpan blockTime;

        //the genesis block construtor
        public Block()
        {
            //initalises the threads
            Thread threadOdd = new Thread(new ThreadStart(threadMine2));
            Thread threadEven = new Thread(new ThreadStart(threadMine1));

            //current time and date
            creationT = DateTime.Now;

            //default positon
            positionInx = 0;
            //no previous hash
            previousHash = "";
            //default difficulty
            difficulty = 3;
            hex = 0;
            //number only used once
            nonce = 0;
            //setting merkle
            merkleRoot = calcMerkl();
            //stop watch for blocktime
            Stopwatch bt = new Stopwatch();
            //starting thread
            threadEven.Start();
            //starting the stopwatch
            bt.Start();
            //starting thread
            threadOdd.Start();
            //wait untill a thread is finished
            while (threadEven.IsAlive && threadOdd.IsAlive) { }
            //kill any thread still running
            threadEven.Abort();
            threadOdd.Abort();
            //stop timer
            bt.Stop();
            //save blocktime
            blockTime = bt.Elapsed;
            //saves hash from winning thread
            currentHash = createHash(nonce);
        }

        public Block(String lastHash, int lastInx, List<transcations> curentTrans,String  minerAddress, int dif,int hexDiff)
        {
            //initales threads
            Thread threadOdd = new Thread(new ThreadStart(threadMine2));
            Thread threadEven = new Thread(new ThreadStart(threadMine1));
            //current time and date
            creationT = DateTime.Now;
            //current position
            positionInx = lastInx + 1;
            //the last blocks hash
            previousHash = lastHash;
            // the tranaction added to the block
            transactionList = curentTrans;
            //current difficulty
            difficulty = dif;
            hex = hexDiff;

            nonce = 0;
            //figures out how much the block is worth
            calcRewards();
            //sets the block to the miner address
            this.minerAddress = minerAddress;
            //creates the mine rewards transaction for the miners
            transcations transaction = new transcations("Mine Rewards", minerAddress, (rewards + fees), 0, "");
            transactionList.Add(transaction);
            //create merkel root
            merkleRoot = calcMerkl();
            //start the threading and the timing 
            Stopwatch bt = new Stopwatch();
            threadEven.Start();
            bt.Start();
            threadOdd.Start();
            while (threadEven.IsAlive && threadOdd.IsAlive){}
            threadEven.Abort();
            threadOdd.Abort();
            bt.Stop();
            //save blocktime
            blockTime = bt.Elapsed;
            //saves hash from winning thread
            currentHash = createHash(nonce);
            
        }

        //same as above but for the incorrect block. it does not set a last hash
        public Block(int lastInx, List<transcations> curentTrans, String minerAddress, int dif)
        {
            Thread threadOdd = new Thread(new ThreadStart(threadMine2));
            Thread threadEven = new Thread(new ThreadStart(threadMine1)) ;

            creationT = DateTime.Now;
            positionInx = lastInx+ 1;
            previousHash = "";
            transactionList = curentTrans;
            difficulty = dif;
            nonce = 0;
            calcRewards();

            this.minerAddress = minerAddress;
            transcations transaction = new transcations("Mine Rewards", minerAddress, (rewards + fees), 0, "");
            transactionList.Add(transaction);

            merkleRoot = calcMerkl();

            Stopwatch bt = new Stopwatch();
            threadEven.Start();
            bt.Start();
            threadOdd.Start();
            while (threadEven.IsAlive && threadOdd.IsAlive) { }
            threadEven.Abort();
            threadOdd.Abort();
            bt.Stop();
            blockTime = bt.Elapsed;
            Console.WriteLine("worked " + nonce + "time taken " + blockTime);
            currentHash = createHash(nonce);
        }

        //fucntion to start the mining for the thread
        private void threadMine1()
        {
            Mine(0);

        }
        private void threadMine2()
        { 
            Mine(1);
        }

        private void Mine(int tnonce)
        {
            int tempNonce = tnonce;
            Boolean difficult = false;
            //runs untill proof of work completed
            while (difficult == false)
            {
                //calcluates a hash
                currentHash = createHash(tempNonce);
                int count = 0;
                //cfinds the amount of zeros 
                for (int i = 0; i < difficulty; i++)
                {
                    char c = currentHash[i];
                    if (c.Equals('0'))
                    {
                        count += 1;
                    }
                }
                //checks to see if it has the correct abount of 0'
                if (count == difficulty)
                {
                    //checks is the next hex character is less than the difficulty hexh
                    if (Convert.ToInt32(currentHash.Substring(count, count-1), 16) <= hex)
                    {
                        nonce = tempNonce;
                        difficult = true;
                    }
                    else
                    {
                        tempNonce += 2;
                    }
                    
                }
                else
                {
                    tempNonce += 2;
                }
            }

        }
     
        //hashing algortihm
        public String createHash(int nonce)
        {
            SHA256 hasher;
            hasher = SHA256Managed.Create();
            //the input with all of the information within the block
            String input = positionInx.ToString() + creationT.ToString() + previousHash + nonce.ToString() + difficulty.ToString() + rewards.ToString() + merkleRoot;
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            String hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;

        }
        private void calcRewards()
        {
            //caclauates how much the block is worth
            rewards += 5;
            foreach (transcations t in transactionList)
            {
                rewards += t.getFee();
            }
           
        }
        public string calcMerkl()
        {
            //combines all of the transaction togetehr to create the merkel root
            String merk1= "";
            String merk2 = "";
            String mkRoot = "";
            switch (transactionList.Count)
            {
                case 1:
                    mkRoot = transactionList[0].getHash();
                    break;
                case 2:
                    mkRoot = HashCode.HashTools.combineHash(transactionList[0].getHash(), transactionList[1].getHash());
                    break;
                case 3:
                    merk1 = HashCode.HashTools.combineHash(transactionList[0].getHash(), transactionList[1].getHash());
                    mkRoot = HashCode.HashTools.combineHash(merk1, transactionList[2].getHash());
                    break;
                case 4:
                    merk1 = HashCode.HashTools.combineHash(transactionList[0].getHash(), transactionList[1].getHash());
                    merk2 = HashCode.HashTools.combineHash(transactionList[2].getHash(), transactionList[3].getHash());
                    mkRoot = HashCode.HashTools.combineHash(merk1,merk2);
                    break;
                case 5:
                    merk1 = HashCode.HashTools.combineHash(transactionList[0].getHash(), transactionList[1].getHash());
                    merk2 = HashCode.HashTools.combineHash(transactionList[2].getHash(), transactionList[3].getHash());
                    merk1 = HashCode.HashTools.combineHash(merk1, merk2);
                    mkRoot = HashCode.HashTools.combineHash(merk1, transactionList[4].getHash());
                    break;
                case 0:
                    break;
            }
            return mkRoot;
        }

        public String getInfo()
        {
            //sens all the block information to the form
            String info = "Date Created: " + creationT 
                + "\nBlock Index: " + positionInx 
                + "\nHash: " + currentHash
                + "\nPrevious Hash: " + previousHash 
                + "\nDifficulty: " + difficulty 
                + "\nNonce: " + nonce 
                + "\nRewards: " + rewards 
                + "\nFees: " + fees 
                + "\nMiner Address: " + minerAddress +"\n\n";
            for (int i = 0; i < transactionList.Count; i++)
            {
                info += "\n\n" + transactionList[i].getInfo();
            }
            return info;
        }
        public String getHash()
        {
            return currentHash;
        }
        public int getNonce()
        {
            return nonce;
        }
        public TimeSpan getDT()
        {
            return blockTime;
        }
        public String getPreviousHash()
        {
            return previousHash;
        }
        public String getMerkle()
        {
            return merkleRoot;
        }
        public double getBalence(String pubKey)
        {
            double balence = 0.0;
            foreach (transcations i in transactionList)
            {
                if (i.getSenderAdd() == pubKey)
                {
                    balence -= i.getAmount();
                }
                else if (i.getRecipientAdd() == pubKey && i.getRecipientAdd() != "Mine Rewards")
                {
                    balence += i.getAmount();
                }
            }
            return balence;
        }
        public int getIndx()
        {
            return positionInx;
        }

        public bool verifyTrans()
        {
            //checks that the infromaotion of the transaction creates a valid digital signiture
            foreach (transcations i in transactionList)
            {
                if (Wallet.Wallet.ValidateSignature(i.getSenderAdd(), i.getHash(), i.getSigniture()) != true)
                {
                    return false;
                }
            }
            return true;
        }
     
    }

}
