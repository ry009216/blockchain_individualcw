﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace BlockchainAssignment
{
    class Blockchain
    {
        //conatins all the blocks
        List<Block> Blocks = new List<Block>();
        //contains the pending trasncations
        List<transcations> transactionPool = new List<transcations>();
        //dictates the type of pool
        int poolType;
        // for addresss based pools
        private String desiredAddress;
        //the number of 0's needed for mines
        private int difficulty = 3;
        //the hex value needed for difficulty
        private int hexDiff = 8; 

        public Blockchain()
        {
            //creates genesis block 
            Blocks.Add(new Block());
            
        }
       
        public String getInfo(int id)
        {
            return Blocks[id].getInfo();
        }
        public String getMostRecentBlock()
        {
            return getInfo(Blocks.Count - 1);
        }
        // creates a new block in the block chain
        public void newBlock(String minerAddress)
        {
            //block index
            int currentSize = Blocks.Count  -1 ;
            //the previous hash
            String lastHash = Blocks[currentSize].getHash();
            //last index
            int lastInx = Blocks[currentSize].getIndx();
            //the pool of transactions to be passed into the new block
            List<transcations> sendingPool = getTransactions();
            if (poolType == 3)
            {
                //gets the transaction from only the desired address
                foreach (transcations i in sendingPool)
                {
                    if (i.getSenderAdd() == desiredAddress)
                    {
                        Blocks.Add(new Block(lastHash, lastInx, sendingPool, minerAddress, difficulty, hexDiff));
                        break;
                    }
                }
            }
            else
            {
                //creates anew block
                Blocks.Add(new Block(lastHash, lastInx, sendingPool, minerAddress,difficulty,hexDiff));
            }
            //checks the block time and adjusts difficulty
            calculateBlockTime();
        }

        
        public List<transcations> getTransactions() 
        {
            List<transcations> sending = new List<transcations>();
            //sends nothing is no trasnactions
            if (transactionPool.Count == 0)
            {
                
            }
            //takes all trasncation is there are less than 5 
            else if(transactionPool.Count < 5)
            {
                int count = transactionPool.Count;
                for (int i = 0;  i < count; i++)
                {
                    //gets the position of the transaciton to be sent from the current pool settin 
                    int pos = settings(poolType);
                    if (pos != -1)
                    {
                        //takes the transactions off the  pool and into the sending pool for the block
                        sending.Add(transactionPool[pos]);
                        transactionPool.RemoveAt(pos);
                    }
                }
            }
            else
            {
                //when the pool size if greater than 5 it will add 5 from the pool
                for (int i = 1; i <= 5; i++)
                {
                    int pos = settings(poolType);
                    if (pos != -1)
                    {
                        sending.Add(transactionPool[pos]);
                        transactionPool.RemoveAt(pos);
                    }
                }
            }
            return sending;
        }

        private int settings(int type)
        {
            switch (type)
            {
                case 0:
                    return altuAlgo();
                case 1:
                    return greedyAlgo();
                case 2:
                    return randAlgo();
                case 3:
                    return adressAlgo(desiredAddress);

            }
            return 0;
        }
        
        private int altuAlgo()
        {
            //finds the algorithm with the oldest timestamp
            int highestInx = 0; 
            for (int i = 1; i < transactionPool.Count; i++)
            {
                if (DateTime.Compare(transactionPool[highestInx].getTimeStamp(),transactionPool[i].getTimeStamp())>0)
                {
                    highestInx = i;
                }
            }
            return highestInx;
        }

        private int greedyAlgo()
        {
            //finds the trancastion with the mout amount of money to earn 
            int highestInx = 0;
            for (int i = 1; i < transactionPool.Count; i++)
            {
                if (transactionPool[i].getFee() > transactionPool[highestInx].getFee())
                {
                    highestInx = i;
                }
            }
            return highestInx;
        }
        private int randAlgo()
        {
            // chooses on at complete random 
            Random rand = new Random();
            int randTran = rand.Next(0, transactionPool.Count);
            return randTran;
        }

        private int adressAlgo(String targetAdd)
        {
            //finds all trasnction with the target address.
            int pos = -1;
            for (int i = 0; i < transactionPool.Count; i ++)
            {
                if (transactionPool[i].getSenderAdd() == targetAdd)
                {
                    pos = i;
                    return pos;
                }
            }
            return pos;
        }


        public String getAllInfo()
        {
            //gets inforamtion about all the blocks
            String results = null;
            for (int i = 0; i < Blocks.Count ; i++)
            {
                results += getInfo(i);
            }
            return results;
        }
       public void calculateBlockTime()
        {
            int lastBlock = Blocks.Count -1;
            //target block time
            double targetBlockTime = 40;
            //the last blocktime
            double blocktime = Blocks[lastBlock].getDT().TotalSeconds;
            
            //reduces the difficulty
            if (blocktime > targetBlockTime + 5)
            {
                if (hexDiff == 15)
                {
                    difficulty--;
                    hexDiff = 0;
                }
                else
                {
                    hexDiff++;
                }
            }
            //increases the difficulty
            else if (blocktime < targetBlockTime - 5)
            {
                if (hexDiff == 0)
                {
                    difficulty++;
                    hexDiff = 15;
                }
                else
                {
                    hexDiff--;
                }
            }
 
        }
     
        public void addTransactionPool (transcations newestTrans)
        {

            transactionPool.Add(newestTrans);
        
        }
        public string printTransactions()
        {
            string result = null;
            foreach (transcations t in transactionPool)
            {
                result +=  t.getInfo() + "\n";
            }
            return result;
        }
        public double clacBalence(String pubKey)
        {
            //finds the ballence of an wallet
            double balence = 0.0;
            //values from block and transcations
            foreach (Block i in Blocks)
            {
                balence += i.getBalence( pubKey);
            }
            //money spent in trasnaction pools but not processed
            foreach (transcations j in transactionPool)
            {
                if (j.getSenderAdd() == pubKey)
                {
                    balence -= j.getAmount();
                }
            }
            return balence;
        }

        public int getTransactionnum()
        {
            return transactionPool.Count;
        }
        public int getSetType() { return poolType; }

        public void setAlgType(int type)
        {
            poolType = type;
        }

        public void setDesiredAddress(String add)
        {
            desiredAddress = add;
        }

        public bool validate()
        {
            //ensures that the blockchainis validates
            for (int i = 0; i < Blocks.Count - 1; i++)
            {
                //all of these must be equal to be valid
               
                if (Blocks[i].getHash() != Blocks[i + 1].getPreviousHash() // checks previous hashses
                    || Blocks[i].getMerkle() != Blocks[i].calcMerkl() //comapres merkles to newley recaluated merkles
                    || Blocks[i].getHash() != Blocks[i].createHash(Blocks[i].getNonce())) // comapres current hash to a newly made hash of the same block
                {
                    
                    return false;
                   
                }
           
            }
            //checks all the transactions
            if (validateTransactions() != true)
            {
                return false;
            }
            return true;
        }

        public bool validateTransactions()
        {
            //checks each transaction in each block
            foreach (Block i in Blocks)
            {
                if (i.verifyTrans() != true)
                {
                    return false;
                }
            }
            return true;
        }
        public void addWrongBlock(string minerAddress)
        {
            //creates an incorrct block with no previous hash
            int currentSize = Blocks.Count - 1;
           
            int lastInx = Blocks[currentSize].getIndx();

            List<transcations> sendingPool = getTransactions();
            if (poolType == 3)
            {
                foreach (transcations i in sendingPool)
                {
                    if (i.getSenderAdd() == desiredAddress)
                    {
                        Blocks.Add(new Block( lastInx, sendingPool, minerAddress, 1));
                        break;
                    }
                }
            }
            else
            {
                Blocks.Add(new Block( lastInx, sendingPool, minerAddress, difficulty));
            }
            //calculateBlockTime();
        }
    }
}
