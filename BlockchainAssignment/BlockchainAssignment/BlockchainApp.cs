﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        //blockchain for the entire application
        Blockchain blockchain;
        //wallets for the BC
        Wallet.Wallet newWallet;
        public BlockchainApp()
        {
            InitializeComponent();
           //intialise the blockahin
            blockchain = new Blockchain();
            //shows the info of the genesis block
            richTextBox1.Text = blockchain.getInfo(0);
           


        }
  
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        // gets the information about a specific block 
        private void button1_Click(object sender, EventArgs e)

        {
            int blockInp = Convert.ToInt32(textBox1.Text);
            
            richTextBox1.Text = blockchain.getInfo(blockInp);
        }

        //creates a new wallet and procues the private and public keys in their respectiv textboxs
        private void walletButton_Click(object sender, EventArgs e)
        {
            
            String privKey;
            newWallet = new Wallet.Wallet(out privKey);
            String publicKey = newWallet.publicID;
            privateBox.Text = privKey;
            publicBox.Text = publicKey;

        }

        //takes the priavte and public keys and sees if they are vaild
        private void valWal_Click(object sender, EventArgs e)
        {

            bool result  = Wallet.Wallet.ValidatePrivateKey(privateBox.Text, publicBox.Text);
            richTextBox1.Text = " the keys are " + result;
        }

        //creates a new transaction
        private void transactionBox_Click(object sender, EventArgs e)
        {
            try
            {
                //checks is the send has enough money to send
                if (blockchain.clacBalence(publicBox.Text) < Convert.ToDouble(amountText.Text))
                {
                    //if not enough funds
                    richTextBox1.Text = "ID: " + publicBox.Text + " has insufficent funds to do this transaction \nTransaction value: "
                        + Convert.ToDouble(amountText.Text) + "\nFunds Available: " + blockchain.clacBalence(publicBox.Text);

                }
                //checks is the keys given are valid 
                else if (Wallet.Wallet.ValidatePrivateKey(privateBox.Text,publicBox.Text) != true)
                {
                    richTextBox1.Text = "The keys given are not valid. Please use another key/s";
                }
                else
                {
                    //creates a new transaction and adds it to the trasnaciton pool
                    transcations newTransaction = new transcations(publicBox.Text, recipientBox.Text, Convert.ToDouble(amountText.Text), Convert.ToDouble(fee.Text), privateBox.Text);
                    richTextBox1.Text = newTransaction.getInfo();
                    blockchain.addTransactionPool(newTransaction);
                }
            }
           catch (Exception x)
            {
                richTextBox1.Text = "Please input the correct information";
            }
        }


        //creates a new block int the block chain
        private void createBut_Click(object sender, EventArgs e)
        {
            if (blockchain.getSetType() == 3)
            {
                blockchain.setDesiredAddress(desiredAddress.Text);
            }
            blockchain.newBlock(recipientBox.Text);
            richTextBox1.Text = blockchain.getMostRecentBlock();

        }

        //shows all the block info in the block chain
        private void printAll_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = blockchain.getAllInfo();
        }

        //shows all the transactions
        private void transactionsBut_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = blockchain.printTransactions();
        }
        // chekcs to see if the blockchains structure is valid
        private void validateBlock_Click(object sender, EventArgs e)
        {
            bool valid = blockchain.validate();
            richTextBox1.Text = "The blockchain is " + valid;
        }

        //checks the current balence of the wallet
        private void balanceCheck_Click(object sender, EventArgs e)
        {
            double balance = blockchain.clacBalence(recipientBox.Text);
            richTextBox1.Text = "ID: "+ recipientBox.Text +" is worth " + balance + " Coins";

        }
        //sets pool type
        private void AltSetBut_Click(object sender, EventArgs e)
        {
            blockchain.setAlgType(0);
        }
        //sets pool type
        private void greSetBut_Click(object sender, EventArgs e)
        {
            blockchain.setAlgType(1);
        }
        //sets pool type
        private void RanSetBut_Click(object sender, EventArgs e)
        {
            blockchain.setAlgType(2);
        }
        //sets pool type
        private void addSetBut_Click(object sender, EventArgs e)
        {
            blockchain.setAlgType(3);
        }
        //creates an purposely incorrect block
        private void wrongBlock_Click(object sender, EventArgs e)
        {
            blockchain.addWrongBlock(recipientBox.Text);
            richTextBox1.Text = blockchain.getMostRecentBlock();
        }
    }
}
